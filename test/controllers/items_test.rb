require 'test_helper'
class ItemsTest < ActionDispatch::IntegrationTest
    setup do
        @list = List.new
    end

    test "create new item without list" do
        post "/api/items", params: {name:'new-item-1',listname:'teste-new-list-1'}
        assert_equal 200, status

        post "/api/lists/check", params: {name:'teste-new-list-1'}
        assert_equal 200, status
    end

    test "check list existence" do
        post "/api/lists/check", params: {name:'teste-new-list-1'}
        assert_equal 204, status

        post "/api/items", params: {name:'new-item-1',listname:'teste-new-list-1'}
        assert_equal 200, status

        post "/api/lists/check", params: {name:'teste-new-list-1'}
        assert_equal 200, status
    end

    test "delete item checked and not checked" do
        post "/api/items", params: {name:'new-item-1',listname:'teste-new-list-1'}
        assert_equal 200, status
        
        @idItem = response.body
        
        delete "/api/items/#{@idItem}"
        assert_equal 400, status
        
        put "/api/items/#{@idItem}", params: {name:'new-item-1',status:true}
        assert_equal 200, status
    end
end