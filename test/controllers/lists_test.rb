require 'test_helper'
class ListsTest < ActionDispatch::IntegrationTest
    include Api

    setup do
        @list = Api::ListsController.new.new('Teste-Sensitive-Case-List')
    end

    test "test uppercase in name list" do
        assert_equal @list.name, 'teste-sensitive-case-list'
    end
end