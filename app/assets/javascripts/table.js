class ItemList extends React.Component {
    constructor(props) {
        super(props);
        
        this.state = {
            id:      this.props.item.id,
            name:    this.props.item.name,
            status:  this.props.item.status,
            key:     this.props.dataKey,
            editing: false
        }
        
        this.changeItem = this.props.changeItem.bind(this);
    }
    
    buy(){
        if(!this.state.editing){
            const self = this;

            if(this.props.item.status)
                var status = false;
            else
                var status = true;
            
            var data = {
                id:     this.state.id,
                status: status,
                name:   this.state.name,
                key: this.state.key
            }
    
            $('.spinner').show('fast');
            $.ajax({
                url: '/api/items/' + data.id,
                type: 'PUT',
                data: data,
                dataType:'json',
            }).done(function(response) {			
    			self.changeItem(data);
                $('.spinner').hide('fast');
            });
        }else{
            this.update();
        }
    }
    
    update(){
        const self = this;
        var data = {
            id:     this.state.id,
            status: this.state.status,
            name:   this.state.name,
            key:    this.state.key
        }

        $('.spinner').show('fast');
        $.ajax({
            url: '/api/items/' + data.id,
            type: 'PUT',
            data: data,
            dataType:'json',
        }).done(function(response) {			
			self.setState({editing:false});
            self.changeItem(data);
            $('.spinner').hide('fast');
        });
    }
    
    changeInput(e) {
        this.setState({name: e.target.value});
    }
    
    edit(){
        if(this.state.editing){
            this.setState({editing:false});
        }else{
            this.setState({editing:true});
        }
    }
    
    delete(){
        const self = this;
        
        var data = {
            id:     this.state.id,
            status: this.state.status,
            name:   this.state.name,
            key:    this.state.key
        }

        $('.spinner').show('fast');
        $.ajax({
            url: '/api/items/' + data.id,
            type: 'DELETE',
            dataType:'json',
        }).done(function(response) {
            self.changeItem(data,true);
            $('.spinner').hide('fast');
        });
    }

    render() {
        return (
            <tr>
              <th scope="row">
                  {this.state.editing ? (<input type="text" className="form-control" value={this.state.name} onChange={this.changeInput.bind(this)}></input>) : this.state.name}
              </th>
              <td>
                <button type="button" onClick={this.buy.bind(this)} className="btn btn-success btn-sm">
                    {this.state.editing ? 'Save' : (this.state.status ? 'Un-Check' : 'Check')}
                </button>
                <button type="button" onClick={this.edit.bind(this)} className="btn btn-secondary btn-sm">
                    {this.state.editing ? 'Cancel' : 'Edit'}
                </button>
                {this.state.status ? (
                    <button type="button" onClick={this.delete.bind(this)} className="btn btn-danger btn-sm">
                        Delete
                    </button>
                ) : '' }
              </td>
              
            </tr>
        );
    }
}

class TableList extends React.Component {
	     constructor(props) {
	        super(props);
			
	        this.state = {
				items:    [],
                listname: '',
                newItem:  ''
			};
	    }
	    
        changeNewItem(e){
            this.setState({newItem: e.target.value});
        }
        
        saveNewItem(){
            const self = this;
    
        $('.spinner').show('fast');
            $.ajax({
                url: '/api/items/',
                type: 'POST',
                data: {name:self.state.newItem,listname:listname},
                dataType:'json',
            }).done(function(response) {	
                var oldItems = self.state.items;
                oldItems.push({
                    id: response,
                    status: false,
                    name:   self.state.newItem
                });
                
                $('.spinner').hide('fast');

                self.setState({
                    items: oldItems,
                    newItem: ''
                });
            });
        }

	    changeItem(data,deleteItem = false) {
            var newItems = this.state.items;
            
            if(deleteItem){
                delete newItems[data.key];
            }else{
                newItems[data.key] = data;
            }

            this.setState({
                items: newItems
            })
	    }
	    
	    componentDidMount() {
            const self = this;
            $('.spinner').show('fast');

	        $.ajax({
	            url: '/api/list/' + listname,
	            type: 'GET',
	            dataType:'json',
	        }).done(function(response) {
	            self.setState({
					items: response
				});

				$('.spinner').hide('fast');
	        });
	    }

	    render() {
            const itemsListTrue = (this.state.items.length > 0 ?
	            (this.state.items.map(
	                (item,key) => (item.status ? <ItemList item={item} changeItem={this.changeItem.bind(this)} key={key} dataKey={key}/> : null))
	            )
	            : null
	        );
            
            const itemsListFalse = (this.state.items.length > 0 ?
	            (this.state.items.map(
	                (item,key) => (!item.status ? <ItemList item={item} changeItem={this.changeItem.bind(this)} key={key} dataKey={key}/> : null))
	            )
	            : null
	        );
            
	        return (
	            <div>
                    <button className="btn btn-primary refresh" type="button" onClick={this.componentDidMount.bind(this)}>Refresh</button>
                    
                    <div className="spinner"><div className="double-bounce1"></div><div className="double-bounce2"></div></div>
                    
                    <div className="main">
                        <div className="input-group mb-3">
                            <div className="input-group-prepend">
                                <span className="input-group-text">New Item</span>
                            </div>
                            <input type="text" className="form-control" value={this.state.newItem} onChange={this.changeNewItem.bind(this)}></input>
                            <div className="input-group-append">
                                <button className="btn btn-primary" type="button" onClick={this.saveNewItem.bind(this)} disabled={this.state.newItem == '' ? 'disabled' : ''}>Add</button>
                            </div>
                        </div>
                        
                        <h4>Not checkeds</h4>
                        <table className="table table-striped">
                            <thead>
                                <tr>
                                    <th scope="col" onClick={this.changeItem.bind(this)}>Item</th>
                                    <th scope="col">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                {itemsListFalse}
                            </tbody>
                        </table>
                        
                        <hr></hr>
                        <h4>Chekeds</h4>
                        <table className="table table-striped">
                            <thead>
                                <tr>
                                    <th scope="col" onClick={this.changeItem.bind(this)}>Item</th>
                                    <th scope="col">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                {itemsListTrue}
                            </tbody>
                        </table>
                    </div>
	            </div>
	        );
	    }
	}

	ReactDOM.render(
	    <TableList />,
	    document.getElementById('root')
	);
    
    
    