class Api::ItemsController < ApplicationController
    skip_before_action :verify_authenticity_token
    before_action only: [:show, :edit, :update, :destroy]

    def create
        @list = List.where(name: params[:listname]).take

        if !@list
            @list = Api::ListsController.new.new(params[:listname])
        end

        @item = Item.new({name:params[:name],list:@list,status:false})
        
        if @item.save
          render json: @item.id, status: 200
        else
          render json: @item.errors, status: :unprocessable_entity
        end
    end

    def update
        @item = Item.find(params[:id])

        if @item.update(name: params[:name], status: params[:status])
            render json: true
        else
            render json: @item.errors, status: :unprocessable_entity
        end
    end

    def destroy            
        begin
            @item = Item.where(id: params[:id], status:true).take
            if @item
                @item.destroy
                render json: true
            else
                render json: false, status: 400
            end
        rescue ActiveRecord::RecordNotFound => e
            render json: false
        end
    end
end