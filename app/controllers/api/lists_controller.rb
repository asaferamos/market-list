class Api::ListsController < ApplicationController
    skip_before_action :verify_authenticity_token
    def check
        @list = List.where(name: params[:name]).take
        if @list
            render json: true.to_json, status: 200
        else
            render json: false, status: 204
        end
    end
    
    def new (listname)
        @list = List.new
        @list.name = listname.downcase
        @list.save
        return @list
    end
    
    def showItemsList
        @items = Item.joins(:list).where(lists: {name: params[:listName]}).order(created_at: :asc)
        if @items
            render json: @items.to_json, status: 200
        else
            render json: false, status: 204
        end
    end
end