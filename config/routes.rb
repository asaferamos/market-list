Rails.application.routes.draw do
    root 'lists#new'

    get '/:listName', to: 'lists#showItemsList'

    namespace :api, constraints: { format: 'json' } do  
        post 'lists/check', to: 'lists#check'
        
        resources :items

        get 'list/:listName', to: 'lists#showItemsList'
    end
end
