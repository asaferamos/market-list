FROM ruby:2.5.1

RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs
RUN mkdir /market-list
WORKDIR /market-list
ADD Gemfile /market-list/Gemfile
ADD Gemfile.lock /market-list/Gemfile.lock
RUN bundle install
ADD . /market-list