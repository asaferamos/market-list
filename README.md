

## Market List
#### Teste de Asafe Ramos para Raise Sistemas

MVP desenvolvido em Rails 5, utilizando React.js na manutenção das informações.

Endpoints consumidos pelo React:


    GET    /api/list/:listName
    POST   /api/lists/check
    POST   /api/items
    PUT    /api/items/:id
    DELETE /api/items/:id

##### Production
[![Deploy](https://img.shields.io/badge/demo--prod-heroku-430098.svg)](https://market-list-asaferamos.herokuapp.com/)

- Deploy automático via Pipeline Bitbucket
- Database PostgreSQL Heroku

##### Development

Testes:

    rails test
 - Database sqlite3
