class AddRelationListItems < ActiveRecord::Migration[5.2]
    def self.up
        add_column :items, :list_id, :integer
    end

    def self.down
        remove_column :items, :list_id
    end
end
